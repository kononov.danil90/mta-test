import { Select as MuiSelect, SelectProps } from '@mui/material';
import './index.css';

function Select<T>(props: SelectProps<T>) {
    return (
        <MuiSelect
            {...props}
            className={`select-form-control ${props.className}`}
        />
    );
};

export { Select };
