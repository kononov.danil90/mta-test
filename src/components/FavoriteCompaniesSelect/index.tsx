import React, { useEffect, useState } from "react";
import {
  CircularProgress,
  FormControl,
  Icon,
  IconButton,
  InputLabel,
  MenuItem,
  SelectChangeEvent,
  SelectProps,
} from "@mui/material";
import "./index.css";

import { Select } from "../Select";
import { Company } from "../../types/Company";
import { CompanyStatsMap } from "../../types/CompanyStats";
import { identity } from "../../helpers/identity";

export const COMPANIES_TYPES: {
  [T in Company['type']]: T;
} = {
  affiliate: "affiliate",
  contract: "contract",
};

type Props = {
  companyType: Array<Company['type']>;
  value: Company['id'][];
  companyStats: CompanyStatsMap;
  companies: Company[];
  favoriteCompanyIds: Company['id'][];
  onChange: (value: Company['id'][]) => void;
  onAddToFavorites: (value: Company['id'][]) => void;
  onRemoveFromFavorites: (value: Company['id'][]) => void;

  variant?: SelectProps['variant'];
  className?: string;
  defaultRenderValue?: string;
  label?: string;
  isShowCountValues?: boolean;
  isFullText?: boolean;
  isHideStats?: boolean;
  getCompanyStats?: (company: Company) => number;
  onClose?: SelectProps['onClose'];
  onOpen?: () => void;
};

type CustomSelectChangeEvent = Omit<SelectChangeEvent, 'target'> & {
  target: Omit<SelectChangeEvent, 'value'> & {
    value: Props['value'];
  };
};

const ALL_DATA_FORMAT = { key: 0, value: "all", title: "All Companies" };

const filterIds = (id: Company['id']) => (ids: Company['id'][]) => (
  ids.includes(id)
    ? ids.filter((item) => item !== id)
    : [...ids, id]
);

type MenuItemProps = {
  id: number;
  title: string;
  icon: string;
  color: string;
  onToggle?: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>, id: MenuItemProps['id']) => void;
  isProcessing?: boolean;
  name?: string;
}

const getMenuItem = ({
  id,
  name,
  title,
  icon,
  color,
  onToggle = identity,
  isProcessing = false,
}: MenuItemProps) => (
  <MenuItem sx={{ pt: "2px", pb: "2px" }} key={id} value={id}>
    <IconButton
      className={"favourite-icon"}
      size={"small"}
      color="primary"
      onClick={(event) => onToggle(event, id)}
    >
      <Icon style={{ color }}>
        {isProcessing ? (
          <CircularProgress style={{ maxWidth: 24, maxHeight: 24 }} />
        ) : (
          icon
        )}
      </Icon>
    </IconButton>
    <div id={String(id)} style={{ width: "100%", padding: "8px 5px" }}>
      {title}
    </div>
  </MenuItem>
);

const getAllInfoMenuItem = () => (
  getMenuItem({
    id: ALL_DATA_FORMAT.key,
    name: ALL_DATA_FORMAT.value,
    title: ALL_DATA_FORMAT.title,
    icon: "star",
    color: "rgb(203, 15, 21)",
  })
);

export const FavoriteCompaniesSelect = (props: Props) => {
  const {
    companyType,
    isShowCountValues,
    value,
    isFullText,
    variant,
    className,
    defaultRenderValue,
    label,
    isHideStats,
    companyStats,
    companies,
    favoriteCompanyIds,
    onChange,
    getCompanyStats,
    onClose,
    onAddToFavorites,
    onRemoveFromFavorites,
    onOpen,
  } = props;

  const [localCheckedFavoriteIds, setLocalCheckedFavoriteIds] = useState<Company['id'][]>([]);
  const [localUncheckedFavoriteIds, setLocalUncheckedFavoriteIds] = useState<Company['id'][]>([]);
  const [companiesByType, setCompaniesByType] = useState<Company[]>([]);

  useEffect(() => {
    setCompaniesByType(
      companyType
        ? companies.filter((company) => companyType.includes(company.type))
        : companies
    );
  }, []);

  const getFavoriteCompanies = () => {
    return companiesByType.filter((company) => favoriteCompanyIds.includes(company.id));
  };

  const getCompanies = () => {
    return companiesByType.filter((company) => !favoriteCompanyIds.includes(company.id));
  }

  const getSelectedCompanies = () => {
    return companiesByType.filter((company) => value.includes(company.id));
  }

  const toggleFavoriteCompanies = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>, id: Company['id']) => {
    event.stopPropagation();
    setLocalCheckedFavoriteIds(filterIds(id));
  };

  const toggleUncheckCompanies = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>, id: Company['id']) => {
    event.stopPropagation();
    setLocalUncheckedFavoriteIds(filterIds(id));
  };

  const getStats = (company: Company) => {
    const basicCompanyStats = companyStats[company.id]?.routes_count || 0

    const stats =
      getCompanyStats
        ? getCompanyStats(company)
        : basicCompanyStats;

    return stats;
  }

  const renderFavoriteItem = (company: Company) => {
    const stats = getStats(company);

    return (
      getMenuItem({
        id: company.id,
        name: company.name,
        title: `${company.name}${isHideStats ? "" : " - " + stats}`,
        icon: localUncheckedFavoriteIds.includes(company.id) ? "star_outline" : "star",
        color: "rgb(255, 179, 0)",
        onToggle: toggleUncheckCompanies,
      })
    );
  };

  const renderItem = (company: Company) => {
    const stats = getStats(company);

    return (
      getMenuItem({
        id: company.id,
        name: company.name,
        title: `${company.name}${isHideStats ? "" : " - " + stats}`,
        icon: localCheckedFavoriteIds.includes(company.id) ? "star" : "star_outline",
        color: "rgba(99,97,97,0.89)",
        onToggle: toggleFavoriteCompanies,
      })
    )
  };

  const getRenderValue = (values: Props['value']) => {
    if (isShowCountValues) {
      if (values.length === 0) {
        return defaultRenderValue || "All";
      }

      return `${values.length} ${isFullText ? (values.length > 1 ? "Companies" : "Company") : "Comp"}`;
    }

    if (values.length === 0 || values.includes(ALL_DATA_FORMAT.key)) {
      return defaultRenderValue || ALL_DATA_FORMAT.title;
    }

    return getSelectedCompaniesNames();
  };

  const getSelectedCompaniesNames = () => {
    const selectedItems = getSelectedCompanies();
    
    return selectedItems
      .map((item) => item.name)
      .join(", ");
  };

  const handleChange = (event: SelectChangeEvent<Props['value']>) => {
    const { target: { value } } = event as unknown as CustomSelectChangeEvent;
    onChange(
      value.at(-1) === ALL_DATA_FORMAT.key
        ? []
        : value
    );
  };

  const handleClose = (event: React.SyntheticEvent) => {
    onAddToFavorites(localCheckedFavoriteIds);
    onRemoveFromFavorites(localUncheckedFavoriteIds);

    setLocalCheckedFavoriteIds([]);
    setLocalUncheckedFavoriteIds([]);

    if (onClose) {
      onClose(event)
    }
  };

  const handleOpen = () => {
    if (onOpen) {
      onOpen();
    }
  };

  return (
    <FormControl
      variant="outlined"
      className={`companies-select ${className}`}
    >
      {label && (
        <InputLabel
          shrink
          htmlFor="company_id"
          style={{ backgroundColor: "white" }}
        >
          {label}
        </InputLabel>
      )}
      <Select<Props['value']>
        label={"favorite companies"}
        className={"companies-select"}
        labelId="favorite_companies_select"
        id="favorite_companies_select_id"
        multiple
        inputProps={{
          name: "company_ids",
        }}
        displayEmpty
        value={value}
        onChange={handleChange}
        renderValue={getRenderValue}
        onClose={handleClose}
        variant={variant || "outlined"}
        onOpen={handleOpen}
      >
        {getAllInfoMenuItem()}
        {getFavoriteCompanies().map(renderFavoriteItem)}
        {getCompanies().map(renderItem)}
      </Select>
    </FormControl>
  );
};
