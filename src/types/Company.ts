export type Company = {
    id: number;
    name: string;
    phone_number: string | null;
    logo: string;
    type: 'affiliate' | 'contract';
    status: 'active';
    users_count: number;
}