export type CompanyStats = {
    id: number;
    routes_count: number;
}

export type CompanyStatsMap = Record<string, CompanyStats>;