import { shallowEqual, useSelector } from "react-redux";
import { Company } from "../types/Company";

type AppState = {
    app: {
        companies: Company[];
    }
};


export const useTypedSelector = <T>(selector: (s: AppState) => T): T => useSelector(selector, shallowEqual);
// export const useTypedDispatch = () => useDispatch<Dispatch<AllActions>>();