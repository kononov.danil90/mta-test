import AppReducer from './app/reducer';

const reducers = {
  app: AppReducer,
};

export default reducers;
