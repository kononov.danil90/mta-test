import { useState } from 'react';
import './App.css';
import { FavoriteCompaniesSelect, COMPANIES_TYPES } from './components/FavoriteCompaniesSelect';
import { Company } from './types/Company';
import company_stats_data from "./mock/compnies_stats.json";
import { useTypedSelector } from './services';
import { actions } from './services/actions';

function App() {
  const [selectedCompanies, setSelectedCompanies] = useState<Company['id'][]>([]);
  const [companyUsers] = useState<Record<string, number>>({
    null: 2738,
    GVC: 40,
    GCS: 4,
    MVT: 25,
    PTC: 46,
    ITA: 25,
    ICP: 18,
    MVP: 18,
    MAG: 78,
  });

  
  
  const companies = useTypedSelector((state) => state.app.companies);
  /**
   * в идеале это лежит в сторе, но в тз про стор ничего не было сказано, так что я туда и не лез.
   * если, все же, селект должен быть редакс-реди компонентом, то можно и инкапсулировать это внутри него.
   * вся логика будет в редьюсере
  */
  const [favoriteCompanyIds, setFavoriteCompanyIds] = useState<Company['id'][]>(actions.appActions.getFavoriteCompanies()());
  const handleAddToFavorites = (ids: Company['id'][]) => {
    setFavoriteCompanyIds((data) => [...data, ...ids]);
  }

  const handleRemoveFromFavorites = (ids: Company['id'][]) => {
    setFavoriteCompanyIds((data) => data.filter((id) => !ids.includes(id)))
  }

  return (
    <div>
      <FavoriteCompaniesSelect
        companyType={[COMPANIES_TYPES.affiliate]}
        isShowCountValues
        isFullText
        variant="standard"
        onChange={setSelectedCompanies}
        value={selectedCompanies}
        companyStats={company_stats_data}
        companies={companies}
        onAddToFavorites={handleAddToFavorites}
        onRemoveFromFavorites={handleRemoveFromFavorites}
        favoriteCompanyIds={favoriteCompanyIds}
      />
      <FavoriteCompaniesSelect
        companyType={[COMPANIES_TYPES.affiliate, COMPANIES_TYPES.contract]}
        className={'driver-companies'}
        defaultRenderValue="All"
        label="Companies"
        value={selectedCompanies}
        onChange={setSelectedCompanies}
        companyStats={company_stats_data}
        companies={companies}
        onAddToFavorites={handleAddToFavorites}
        onRemoveFromFavorites={handleRemoveFromFavorites}
        favoriteCompanyIds={favoriteCompanyIds}
      />
      <FavoriteCompaniesSelect
        isFullText
        companyType={[COMPANIES_TYPES.contract]}
        defaultRenderValue={'All Companies'}
        className={'header-rides__companies'}
        isShowCountValues
        variant="standard"
        value={selectedCompanies}
        onChange={setSelectedCompanies}
        companyStats={company_stats_data}
        companies={companies}
        onAddToFavorites={handleAddToFavorites}
        onRemoveFromFavorites={handleRemoveFromFavorites}
        favoriteCompanyIds={favoriteCompanyIds}
      />
      <FavoriteCompaniesSelect
        companyType={[COMPANIES_TYPES.affiliate]}
        isShowCountValues
        isFullText
        variant="standard"
        value={selectedCompanies}
        onChange={setSelectedCompanies}
        defaultRenderValue={'All Companies'}
        companyStats={company_stats_data}
        companies={companies}
        onAddToFavorites={handleAddToFavorites}
        onRemoveFromFavorites={handleRemoveFromFavorites}
        favoriteCompanyIds={favoriteCompanyIds}
      />
      <FavoriteCompaniesSelect
        isFullText
        companyType={[COMPANIES_TYPES.affiliate]}
        defaultRenderValue={'All Companies'}
        className={'header-rides__companies'}
        variant="standard"
        value={selectedCompanies}
        onChange={setSelectedCompanies}
        getCompanyStats={company => companyUsers[company.name] || 0}
        companyStats={company_stats_data}
        companies={companies}
        onAddToFavorites={handleAddToFavorites}
        onRemoveFromFavorites={handleRemoveFromFavorites}
        favoriteCompanyIds={favoriteCompanyIds}
      />
    </div>
  );
}

export default App;
