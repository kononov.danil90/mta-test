## Skill Assessment Test

The task at hand is to correct types and refactor the `FavoriteCompaniesSelect` component - converting it to TypeScript and ensuring clean and understandable code. Currently, it's quite tangled, though the component itself is straightforward.

At present, the component functions correctly. In the App.tsx file, there are several usage examples - done to illustrate how the component should function. After refactoring, it should behave the same way. Prop names can be changed if it improves code readability.

In the `FavoriteCompaniesSelect` file, there's a constant `company_stats` - for testing purposes, it's data from a JSON, but in real scenarios, it will be dynamic. Therefore, this data should be passed into the component's props.

If achieving ideal code necessitates rewriting the component entirely from scratch - no problem.

## The aim of this test is to produce concise, understandable, and correctly functioning code.

# Тестовое задание для оценки навыков

Суть задачи - нужно исправить типы, и отрефакторить компонент `FavoriteCompaniesSelect` - перевести на Typescript и сделать там чистый и понятный код. В данный момент там чёрт ногу сломит, а компонент на самом деле очень простой.

В данный момент компонент работает правильно. В файле App.tsx есть несколько примеров использования - это сделано для того, чтобы можно было понять, как должен работать компонент. После рефакторинга он должен работать так же.
Названия пропсов могут быть другие, если это улучшит читаемость кода.

В файле `FavortieCompaniesSelect` есть константа `company_stats` - в рамках тестового это данные из json, но в реальных условиях они будут динамические, поэтому эти данные должны приходить в пропсы компонента.

Если для достижения идеального кода нужно будет полностью переписать компонент с нуля - без проблем.

## Цель тестового задания - очень краткий, понятный и правильно работающий код.